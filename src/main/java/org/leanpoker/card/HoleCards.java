package org.leanpoker.card;

import java.util.List;

public class HoleCards {
	
	private List<Card> cards;

	public HoleCards(List<Card> cards) {
		super();
		this.cards = cards;
	}
	
	public boolean isSuitTheSame() {
		return cards.get(0).getSuit().equals(cards.get(1).getSuit());
	}
	
	public boolean isRankTheSame() {
		return cards.get(0).getRank().equals(cards.get(1).getRank());
	}
	
	
	public Card getFirst() {
		return cards.get(0);
	}
	
	public Card getSecond() {
		return cards.get(1);
	}
	
	

}
