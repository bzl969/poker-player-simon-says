package org.leanpoker.card;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class GameStateUtil {

    public static final String HOLE_CARDS = "hole_cards";
    
	public static HoleCards getHoleCards(JsonElement request) {
		List<Card> cards = new ArrayList<>();
		Iterator<JsonElement> playersIterator = request.getAsJsonObject().getAsJsonArray("players").iterator();
        JsonArray holeCards = null;
        while (holeCards == null && playersIterator.hasNext()) {
            JsonObject player = playersIterator.next().getAsJsonObject();
            if (player.has(HOLE_CARDS)) {
                holeCards = player.getAsJsonArray(HOLE_CARDS);
            }
        }
        Iterator<JsonElement> holdCardsIterator = holeCards.iterator();

        while (holdCardsIterator.hasNext()) {
            JsonObject card = holdCardsIterator.next().getAsJsonObject();
            cards.add(new Card(card.get("suit").getAsString(), card.get("rank").getAsString()));
        }
        
        return new HoleCards(cards);
	}
	
	public static int getIntegerForStartingHand(HoleCards holeCards) {
		if (holeCards.getFirst().getIndexOfRank() > 8 && holeCards.isRankTheSame()) {
			return 1;
		} else if (holeCards.getFirst().getIndexOfRank() > 10 && holeCards.getSecond().getIndexOfRank() > 10 && holeCards.isSuitTheSame()) {
			return 1;
		} else if (holeCards.getFirst().getIndexOfRank() > 10 && holeCards.getSecond().getIndexOfRank() > 10) {
			return 2;
		} else if (holeCards.getFirst().getIndexOfRank() > 9 && holeCards.getSecond().getIndexOfRank() > 9 && holeCards.isSuitTheSame() ) {
			return 2;
		} else if (holeCards.getFirst().getIndexOfRank() > 8 && holeCards.getSecond().getIndexOfRank() > 8 && holeCards.isSuitTheSame()) {
			return 2;
		} else if (holeCards.getFirst().getIndexOfRank() == 8 && holeCards.isRankTheSame()) {
			return 2;
		} else if (holeCards.getFirst().getIndexOfRank() == 7 && holeCards.isRankTheSame()) {
			return 3;
		} else if (holeCards.getFirst().getIndexOfRank() == 6 && holeCards.isRankTheSame()) {
			return 4;
		} 
		
		return 6;
	}

}
