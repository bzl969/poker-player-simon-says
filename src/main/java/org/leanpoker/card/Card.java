package org.leanpoker.card;

public class Card {
	
	private String suit;
	private String rank;
	
	public Card(String suit, String rank) {
		this.suit = suit;
		this.rank = rank;
	}
	public String getSuit() {
		return suit;
	}
	public void setSuit(String suit) {
		this.suit = suit;
	}
	public String getRank() {
		return rank;
	}
	public void setRank(String rank) {
		this.rank = rank;
	}
	
	public int getIndexOfRank() {
		for (int i=0; i< 13; i++) {
			if (ranks[i].equals(rank)) {
				return i;
			}
		}
		return 0;
	}

	//private static String[] suits = { "hearts", "spades", "diamonds", "clubs" };
	private static String[] ranks = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A" };

	
}
