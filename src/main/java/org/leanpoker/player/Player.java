package org.leanpoker.player;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.leanpoker.card.GameStateUtil;
import org.leanpoker.card.HoleCards;

public class Player {
    static final String VERSION = "Default Java folding player";
    public static final String HOLE_CARDS = "hole_cards";
    public static final Map<Integer, Integer> HANDS_TO_BET = new HashMap<>(5);

    static {
        HANDS_TO_BET.put(1, 1000000);
        HANDS_TO_BET.put(2, 1000000);
        HANDS_TO_BET.put(3, 500);
        HANDS_TO_BET.put(4, 200);
        HANDS_TO_BET.put(5, 100);
    }

    public static int betRequest(JsonElement request) {
        HoleCards holeCards = GameStateUtil.getHoleCards(request);
        int startingHand = GameStateUtil.getIntegerForStartingHand(holeCards);
        Integer bet = HANDS_TO_BET.get(startingHand);
        if (bet != null) {
            return bet;
        } else {
            return 0;
        }
        // System.err.println(request);
        // if (goodCards(request)) {
        // return 1000000;
        // } else {
        // return 0;
        // }
    }

    private static boolean goodCards(JsonElement request) {
        boolean goodCards = false;
        Iterator<JsonElement> playersIterator = request.getAsJsonObject().getAsJsonArray("players").iterator();
        JsonArray holeCards = null;
        while (holeCards == null && playersIterator.hasNext()) {
            JsonObject player = playersIterator.next().getAsJsonObject();
            if (player.has(HOLE_CARDS)) {
                holeCards = player.getAsJsonArray(HOLE_CARDS);
            }
        }
        Iterator<JsonElement> holdCardsIterator = holeCards.iterator();

        while (!goodCards && holdCardsIterator.hasNext()) {
            JsonObject card = holdCardsIterator.next().getAsJsonObject();
            String rank = card.get("rank").getAsString();
            if (rank.equals("A") || rank.equals("K")) {
                goodCards = true;
            }
        }
        return goodCards;
    }

    public static void showdown(JsonElement game) {
    }
}
